libgeo-ipfree-perl (1.160000-2) unstable; urgency=medium

  [ Debian Janitor ]
  * Apply multi-arch hints. + libgeo-ipfree-perl: Add Multi-Arch: foreign.

 -- Jelmer Vernooĳ <jelmer@debian.org>  Thu, 13 Oct 2022 18:23:13 +0100

libgeo-ipfree-perl (1.160000-1) unstable; urgency=medium

  [ Salvatore Bonaccorso ]
  * debian/control: Use HTTPS transport protocol for Vcs-Git URI

  [ gregor herrmann ]
  * debian/copyright: change Copyright-Format 1.0 URL to HTTPS.
  * Remove Brian Cassidy from Uploaders. Thanks for your work!
  * Remove Jonathan Yu from Uploaders. Thanks for your work!

  [ Salvatore Bonaccorso ]
  * Update Vcs-* headers for switch to salsa.debian.org

  [ gregor herrmann ]
  * debian/watch: use uscan version 4.

  [ Debian Janitor ]
  * Trim trailing whitespace.
  * Bump debhelper from old 9 to 12.
  * Set debhelper-compat version in Build-Depends.
  * Set upstream metadata fields: Bug-Database, Bug-Submit, Repository,
    Repository-Browse.
  * Bump debhelper from old 12 to 13.

  [ gregor herrmann ]
  * Import upstream version 1.160000.
  * Update debian/upstream/metadata.
  * Update debian/copyright: Upstream-Contact, and copyright years.
  * Declare compliance with Debian Policy 4.6.0.
  * Set Rules-Requires-Root: no.
  * Annotate test-only build dependencies with <!nocheck>.

 -- gregor herrmann <gregoa@debian.org>  Fri, 04 Feb 2022 20:42:26 +0100

libgeo-ipfree-perl (1.151940-1) unstable; urgency=medium

  [ Salvatore Bonaccorso ]
  * Update Vcs-Browser URL to cgit web frontend

  [ gregor herrmann ]
  * New upstream release.
  * Update years of copyright.
  * Mark package as autopkgtest-able.
  * Declare compliance with Debian Policy 3.9.6.
  * Bump debhelper compatibility level to 9.

 -- gregor herrmann <gregoa@debian.org>  Sat, 31 Oct 2015 18:34:43 +0100

libgeo-ipfree-perl (1.141670-1) unstable; urgency=medium

  * New upstream release.
  * Strip trailing slash from metacpan URLs.
  * Drop spelling.patch, merged upstream.
  * Update third-party copyright years.

 -- gregor herrmann <gregoa@debian.org>  Thu, 19 Jun 2014 17:16:34 +0200

libgeo-ipfree-perl (1.140470-1) unstable; urgency=medium

  [ Nuno Carvalho ]
  * debian/control: remove Gustavo Franco from Uploaders list
    (Closes: #729408).

  [ gregor herrmann ]
  * debian/control: remove Nicholas Bamber from Uploaders on request of
    the MIA team.
  * New upstream release.
  * Update years of copyright and license text.
  * Add a patch to fix a spelling mistake.
  * Declare compliance with Debian Policy 3.9.5.

 -- gregor herrmann <gregoa@debian.org>  Mon, 17 Feb 2014 20:00:08 +0100

libgeo-ipfree-perl (1.132870-1) unstable; urgency=low

  * New upstream release.
  * Update license wording.

 -- gregor herrmann <gregoa@debian.org>  Fri, 18 Oct 2013 19:11:47 +0200

libgeo-ipfree-perl (1.131650-1) unstable; urgency=low

  [ Salvatore Bonaccorso ]
  * Change Vcs-Git to canonical URI (git://anonscm.debian.org)
  * Change search.cpan.org based URIs to metacpan.org based URIs

  [ gregor herrmann ]
  * New upstream release.
  * debian/copyright: update years of copyright and license terms.
  * Declare compliance with Debian Policy 3.9.4.

 -- gregor herrmann <gregoa@debian.org>  Wed, 02 Oct 2013 23:54:37 +0200

libgeo-ipfree-perl (1.121660-1) unstable; urgency=low

  * New upstream release.
  * debian/copyright: update to Copyright-Format 1.0.
  * Update copyright years.
  * Remove unused lintian override.
  * Bump Standards-Version to 3.9.3 (no changes).
  * Set debhelper compatibility level to 8.

 -- gregor herrmann <gregoa@debian.org>  Fri, 15 Jun 2012 14:58:12 +0200

libgeo-ipfree-perl (1.120460-1) unstable; urgency=low

  * New upstream release

 -- Nicholas Bamber <nicholas@periapt.co.uk>  Fri, 17 Feb 2012 16:23:08 +0000

libgeo-ipfree-perl (1.112870-1) unstable; urgency=low

  * New upstream release

 -- Nicholas Bamber <nicholas@periapt.co.uk>  Sat, 15 Oct 2011 17:10:29 +0100

libgeo-ipfree-perl (1.112490-1) unstable; urgency=low

  [ Ansgar Burchardt ]
  * debian/control: Convert Vcs-* fields to Git.

  [ Salvatore Bonaccorso ]
  * debian/copyright: Replace DEP5 Format-Specification URL from
    svn.debian.org to anonscm.debian.org URL.

  [ Nicholas Bamber ]
  * New upstream release

 -- Nicholas Bamber <nicholas@periapt.co.uk>  Sun, 11 Sep 2011 10:42:23 +0100

libgeo-ipfree-perl (1.111650-1) unstable; urgency=low

  * Team upload.
  * New upstream release
  * Gunnar Wolf <gwolf@debian.org> removed from Uploaders. See
    http://lists.debian.org/debian-perl/2011/03/msg00004.html
  * Bump Standards-Version to 3.9.2 (no changes needed).

 -- Salvatore Bonaccorso <carnil@debian.org>  Wed, 15 Jun 2011 07:57:11 +0200

libgeo-ipfree-perl (1.110450-1) unstable; urgency=low

  [ Nicholas Bamber ]
  * New upstream release
  * Updated copyright

  [ Salvatore Bonaccorso ]
  * debian/copyright: Update copyright years for upstream part.

 -- Nicholas Bamber <nicholas@periapt.co.uk>  Wed, 16 Feb 2011 01:09:31 +0000

libgeo-ipfree-perl (1.102870-1) unstable; urgency=low

  * New upstream release
  * Added myself to Uploaders
  * Refreshed copyright
  * Upped standards version to 3.9.1

 -- Nicholas Bamber <nicholas@periapt.co.uk>  Sat, 16 Oct 2010 10:09:49 +0100

libgeo-ipfree-perl (1.101650-1) unstable; urgency=low

  * New upstream release.
    - Updated database: Mon Jun 14 06:40:01 2010 UTC
  * Convert to source format 3.0 (quilt).
  * Update years of packaging copyright.

 -- gregor herrmann <gregoa@debian.org>  Mon, 21 Jun 2010 22:11:14 +0200

libgeo-ipfree-perl (1.100470-1) unstable; urgency=low

  * New upstream release
    + Updated database as of 16 Feb 2010, 06:40:01 UTC
  * Standards-Version 3.8.4 (no changes)
  * Update copyright information to new DEP5 format

 -- Jonathan Yu <jawnsy@cpan.org>  Tue, 16 Feb 2010 21:59:33 -0500

libgeo-ipfree-perl (0.8-1) unstable; urgency=low

  [ Jonathan Yu ]
  * New upstream release
    + Updated database as of 7 Oct 2009, 06:40:02 UTC

  [ gregor herrmann ]
  * Add libtest-pod-perl, libtest-pod-coverage-perl to Build-Depends-Indep for
    the new tests.

 -- Jonathan Yu <jawnsy@cpan.org>  Wed, 07 Oct 2009 16:11:23 -0400

libgeo-ipfree-perl (0.7-1) unstable; urgency=low

  [ Jonathan Yu ]
  * New upstream release
    + Updated database as of 8 Sep 2009, 06:40:01 UTC
  * Standards-Version 3.8.3 (drop perl version dependency)
  * Add myself to Uploaders and Copyright
  * Refreshed copyright
  * Rewrote control description

  [ Nathan Handler ]
  * debian/watch: Update to ignore development releases.

  [ gregor herrmann ]
  * Update debian/README.Debian (misc/webgeo2ipct.pl was removed, the database
    is available for download in the suitable format directly now).
  * Update license information for lib/Geo/ipscountry.dat in debian/copyright.

 -- Jonathan Yu <jawnsy@cpan.org>  Mon, 21 Sep 2009 19:44:37 -0400

libgeo-ipfree-perl (0.6-1) unstable; urgency=low

  * New upstream release
  * debian/control: Standards-Version to 3.8.1

 -- Brian Cassidy <brian.cassidy@gmail.com>  Mon, 11 May 2009 22:26:31 -0300

libgeo-ipfree-perl (0.5-1) unstable; urgency=low

  * New upstream release.
  * debian/copyright: update years of packaging copyright.

 -- gregor herrmann <gregoa@debian.org>  Fri, 13 Feb 2009 14:57:25 +0100

libgeo-ipfree-perl (0.4-1) unstable; urgency=low

  * New upstream release.
  * debian/control: add myself to Uploaders

 -- Brian Cassidy <brian.cassidy@gmail.com>  Mon, 01 Dec 2008 16:42:36 -0400

libgeo-ipfree-perl (0.3-1) unstable; urgency=low

  * New upstream release:
    - remove debian/webgeo2ipct.pl, now shipped in upstream distribution
    - remove patch and quilt framework, fixed in upstream distribution
    - remove debian/README.source, since we don't change the upstream source
      any more
  * debian/copyright: switch to new format.
  * Set debhelper compatibility level to 7; adapt
    debian/{control,compat,rules}; drop cdbs.
  * Add debian/README.Debian to explain how to manually update the country
    data file.
  * Don't compress example scripts.
  * debian/control:
    - improve short description
    - add ${misc:Depends} to Depends:

 -- gregor herrmann <gregoa@debian.org>  Sat, 29 Nov 2008 20:23:25 +0100

libgeo-ipfree-perl (0.2-7) unstable; urgency=low

  * Provide an updated ipscountry.dat (country data file); thanks to Claus
    Herwig for the bug report and the tools and hints;
    - add debian/README.source to explain how to update ipscountry.dat
    - add debian/webgeo2ipct.pl by Claus Herwig (utility script for producing an
      updated ipscountry.dat file)
    - debian/copyright: information about the two new files
    - add patch fix_tests.patch to adjust the tests to the changed database
    - add debian/NEWS.Debian to document that the return value for three
      reserved address ranges has changed
    Closes: #505736
  * Set Standards-Version to 3.8.0, mention quilt usage in
    debian/README.source.
  * debian/control: change my email address.
  * debian/control: Changed: Switched Vcs-Browser field to ViewSVN
    (source stanza).

 -- gregor herrmann <gregoa@debian.org>  Sun, 16 Nov 2008 23:42:53 +0100

libgeo-ipfree-perl (0.2-6) unstable; urgency=low

  * debian/control: Added: Vcs-Svn field (source stanza); Vcs-Browser
    field (source stanza); Homepage field (source stanza). Removed: XS-
    Vcs-Svn fields.
  * debian/rules: delete /usr/lib/perl5 only if it exists (closes: #468033).
  * debian/watch: use dist-based URL.
  * Set Standards-Version to 3.7.3 (no changes).
  * debian/copyright: use version-agnostic download URL.
  * Remove debian/docs, Changes and README get installed by some CDBS magic
    anyway.

 -- gregor herrmann <gregor+debian@comodo.priv.at>  Sat, 08 Mar 2008 15:10:15 +0100

libgeo-ipfree-perl (0.2-5) unstable; urgency=low

  * Apply patch from Ubuntu: Remove bashisms in debian/rules.
  * Remove empty /usr/lib/perl5 directory from binary package.
  * Add missing parts to debian/copyright.

 -- gregor herrmann <gregor+debian@comodo.priv.at>  Tue, 11 Sep 2007 16:50:49 +0200

libgeo-ipfree-perl (0.2-4) unstable; urgency=low

  * Dropped the previous maintainer's README.Debian, which led to
    believe this package was experimental and not officialy from Debian
  * Moving ipct2txt.pl and txt2ipct.pl to /usr/share/doc/libgeo-ipfree-
    perl/examples

 -- Gunnar Wolf <gwolf@debian.org>  Fri,  9 Mar 2007 17:38:02 -0600

libgeo-ipfree-perl (0.2-3) unstable; urgency=low

  * Moved debhelper and cdbs to Build-Depends.
  * Set Standards-Version to 3.7.2 (no changes).
  * Set Debhelper Compatibility Level to 5.

 -- gregor herrmann <gregor+debian@comodo.priv.at>  Fri, 16 Jun 2006 15:58:08 +0200

libgeo-ipfree-perl (0.2-2) unstable; urgency=low

  * debian/control:
    - Maintainer field changed to Debian Perl Group
    - Uploaders field added

 -- Gustavo Franco <stratus@debian.org>  Fri, 17 Jun 2005 23:14:49 -0300

libgeo-ipfree-perl (0.2-1) unstable; urgency=low

  * Initial Release.

 -- Gustavo Franco <stratus@acm.org>  Mon, 28 Jun 2004 16:09:12 -0300
